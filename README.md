With ScoreFollower4L, you can let Ableton or Max follow your playing without having to worry about triggering events or aligning to a click-track. Ideal for live music performances using backup tracks, live electronics, improvisation, or music that requires any live triggering.

Advanced Score Follower for Ableton Live using the [Antescofo module](https://forum.ircam.fr/projects/detail/antescofo/). 

If you enjoy the software, please consider making a donation [here](https://www.paypal.com/donate/?hosted_button_id=X8G45PFSVWDDA)


Watch tutorial [here](https://youtu.be/-MrDAuWy7LU).


# Getting started

## Installation

Download the folder, and drag the .amxd file into your Ableton session. Some quarantining for the Bach and Antescofo libraries might be required for Apple M1+ computers. Ableton Live 11 or more is required to be able to run MIDI in and out of the device.

## Load a Score

Export your MIDI file and convert it using Antescofo's [Online converter](https://antescofo-converter.ircam.fr) or by tapping on the ScoreFollower logo. Click on "Load Score", and drag the antescofo .txt file to the file drop. You can also single tap on the file drop for a dialogue to load your file.

If you click on the button next to the "text score” icon, that should push any changes you make on text to the Bach score.

## Adjust the Input and MIDI Output

Check that your audio inputs and outputs are enabled in Live's settings. Go to the "Input" and "Advanced" Tab, and tweak your audio levels and listening parameters. Atfer that, choose the track you want to send your MIDI values out.

## Follow the Score

Click on "Following On", and the play button. This means the score follower machine is listening to you. Whenever you want to turn the Following Off (like for instance, in a cadence or improvisation), you can turn Following Off by adding automation, or adding `suivi "on"` or `suivi "off"` in the score. 

## Score Navigation

You can skip the score by either **Events** (notes, chords, trills, etc) or **Labels** (bar numbers, rehearsal marks, etc) with the forward/backward arrows. This only works when the score "Follower" is off. 

## Text Score

Click the "Text Score” button to open up the text score on a file. In order to edit text scores, it is reccommended to use a separate editor, such as TextEdit. 

If you click on the button next to the "Text Score” button, that should push any changes you make on text score to the Bach score.


# The Editor Tab

## Event Specifications

Navegate to the desired note on your score by clicking on the forward/backward arrows, and you will see the it in text format in the Editor window. Select your desired event specification from the menu and click on the button to push the changes to the Antescofo text score. This will be then updated inside the Text Score. 

💡 Note that this method does not support rests as events. To attach information to rests, do it manually inside the "Text Score" button. Also, in order to delete any text, you should also do it from the text score window. 

## Triggering things in Live

Go to the "Messages" section.

This section contains a menu with code snippets. Copy paste the `@fun_def` line into the beginning of the Text score, and substitute the name of your [function](https://antescofo-doc.ircam.fr/Reference/functions_def/) and the name of your Max receiver in Live. Then, you can pass a number to the function and this number is going to be sent to the receiver. 

A simple example: Let's trigger a specific scene in Live. 

First, put this at the top of your text file:

`@fun_def @scene($value) { slot1 value }`

Second, place your "Scene Launcher" device next to ScoreFollower4l and set the receiver to be `slot1`.

Then, trigger the Scene number 3 in your desired note just by writing:

```
NOTE C# 1/2
@scene(3) ; this means that scene 3 will be triggered by being sent into the slot1 receiver in Live
```

This is just a simple example. You can pass any number or information to any reciever in Live. For this, I reccommend using the free [MultiMap Receiver](https://maxforlive.com/library/device/8060/multimap-receiver) device, that I made for this purpose. You need to set whatever name you want your function and give the same name to your MultiMap reciever device.


# Advanced Uses

## Advanced Settings

A list of advanced calibration settings is provided. These are pre-defined when a score is loaded, and sent directly to Antescofo, and can also be modified or automated in realtime. 

To know more about what these mean, hover over the sliders and look at the Info window in live, or read the [Antescofo Reference](https://antescofo-doc.ircam.fr/Reference/event_ref/).

💡 You can always check the current status of your settings by clicking on the "Status Window" underneath.

## Realtime messages

If you'd like to send messages to Antescofo on the fly, toggle the "Send" button on, and everytime there is a change in the text box, the script will be sent directly into the Antescofo listening machine. 


# Support & Development 

## To-Do

At the moment, the biggest improvement would be to add a nodejs script to interface with Antescofo's online converter, so that the conversion is done internally within the device. It would also be nice to add a way of inputting text into the text file directly from Max, to ease out code knowledge.

## Further Questions

If you have further questions, do not hesitate to contact me through the Ircam Forum or my [website](http://www.arnaubrichs.com), and contribute to the project.

## Acknowledgements

Thanks to Jean-Louis Giavitto for his invaluable help in developing this project.
